
import java.io.*;
import java.math.BigDecimal;
import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;
/* https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/package-summary.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/index.html?org/apache/commons/math3/util/Precision.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/MathUtils.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/Precision.html
*/

enum CrossoverType {
    UNIFORM,
    EXPONENTIAL
}


class F4CostFunc {

    int variablesNumber;
    int N;
    double[][] x;
    double[] y;

    double c = 1.0;
    

    public F4CostFunc(String path) {
        this.variablesNumber = 6;

        try {

            //ovo samo preobroji broj redova
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int examplesCounter = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                examplesCounter++;
            }
            N = examplesCounter;

            myReader.close();

            x = new double[examplesCounter][5];
            y = new double[examplesCounter];

            myObj = new File(path);
            myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                data = data.substring(1, data.length() - 1);
                String[] dataSplitted = data.split(", ");
                for (int j = 0; j < dataSplitted.length-1; j++) {
                    x[i][j] = Double.parseDouble(dataSplitted[j]);
                }
                //String debugVar2 = dataSplitted[dataSplitted.length-1];
                //BigDecimal DebugVar = new BigDecimal("-1231232171.366999794798722313213");
                /* double debugVar1 = double.parseDouble("1.2345678912345678912345678");
                double debugVar2 = double.parseDouble("1.2345678912345678912345678");
                System.out.println(debugVar1);
                System.out.println(debugVar2);
                double debugVar3 = double.parseDouble(dataSplitted[dataSplitted.length-1]);
                System.out.println(debugVar3); */
                y[i] = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                i++;
            }
            //System.out.println(x);
            //System.out.println(y);
            myReader.close();
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
    }

    
    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    
    public double GetValueAtPoint(double[] v) {
        double sum = 0.0f;
        for (int i = 0; i < N; i++) {
            
            //sum += Math.pow(y_function(v, x[i]) - y[i], 2)*c;
            sum += Math.abs(y_function(v, x[i]) - y[i])*c; //korištena je absolutna vrijednost jer je manja, pa da ne ode sve u inf
        }
        sum = sum / N;
        return sum;
    }

    public static double y_function(double[] v, double[] x) {
        double result = v[0]*x[0] + 
                       v[1]*(double)Math.pow(x[0], 3)*x[1] + 
                       v[2]*(double)Math.exp(v[3]*x[2])*(1 + (double)Math.cos(v[4]*x[3])) + 
                       v[5]*x[3]*(double)Math.pow(x[4],2);

        return result;
        
    }
}

class Optimizer {

    F4CostFunc costFunc;
    int populationSize;
    int maxGenerations;
    double F;
    double Cr;
    CrossoverType crossoverType;
    double rangeMin;
    double rangeMax;	


    public Optimizer(F4CostFunc costFunc, int populationSize, int maxGenerations, double rangeMin, double rangeMax, double F, double Cr, CrossoverType crossoverType) {
        this.costFunc = costFunc;
        this.populationSize = populationSize;
        this.maxGenerations = maxGenerations;
        this.rangeMin = rangeMin;
        this.rangeMax = rangeMax;
        this.F = F;
        this.Cr = Cr;
        this.crossoverType = crossoverType;
    }

    public double[] Optimize() {

        double[][] solutionsPopulation = GenerateStartingPopulation();
        System.out.println(AverageCostFunction(solutionsPopulation));

        for (int generationCounter = 0; generationCounter < maxGenerations; generationCounter++) {
            double[][] newSolutionsPopulation = new double[populationSize][costFunc.variablesNumber];
            for(int i = 0; i < solutionsPopulation.length; i++) {
                double[] targetVector = solutionsPopulation[i];
                int baseVectorIndex = RandomIndexFromPopulation(new int[]{i});
                double[] baseVector = solutionsPopulation[baseVectorIndex];
                int r1 = RandomIndexFromPopulation(new int[]{i, baseVectorIndex});
                int r2 = RandomIndexFromPopulation(new int[]{i, baseVectorIndex, r1});
                double[] r1Vector = solutionsPopulation[r1];
                double[] r2Vector = solutionsPopulation[r2];
                
                double[] mutantVector = AddVectors(baseVector, MultiplyVectorByScalar(AddVectors(r1Vector, MultiplyVectorByScalar(r2Vector, -1.0)), F));

                double[] trialVector = VectorCrossover(mutantVector, targetVector, crossoverType, Cr);

                if (costFunc.GetValueAtPoint(trialVector) <= costFunc.GetValueAtPoint(targetVector)) {
                    newSolutionsPopulation[i] = trialVector;
                } else {
                    newSolutionsPopulation[i] = targetVector;
                }
            }
            solutionsPopulation = newSolutionsPopulation;

            System.out.println(AverageCostFunction(solutionsPopulation));
        }

        return ReturnBestSolutionInPopulation(solutionsPopulation);

    }


    int RandomIndexFromPopulation(int[] forbiddenIndexes) {
        while (true) {
            Random r = new Random();
            int randomNum = r.nextInt(populationSize); //https://www.geeksforgeeks.org/java-util-random-nextint-java/
            if (!Arrays.asList(forbiddenIndexes).contains(randomNum)){ //https://stackoverflow.com/questions/1128723/how-do-i-determine-whether-an-array-contains-a-particular-value-in-java
                return randomNum;
            }
        }
    }
    double[][] GenerateStartingPopulation() {
        double[][] population = new double[populationSize][costFunc.variablesNumber];

        for (int i = 0; i < populationSize; i++) {
            for (int d = 0; d < costFunc.variablesNumber; d++) {
                Random r = new Random();
                double randomValue = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
                population[i][d] = randomValue;
            }
        }

        return population;
    }

    
    double[] AddVectors(double[] v1, double[] v2) {
        double[] resultVector = new double[v1.length];
        for (int i = 0; i < v1.length; i++) {
            resultVector[i] = v1[i] + v2[i];
        }
        return resultVector;
    }

    double[] MultiplyVectorByScalar(double[] v1, double scalar) {
        double[] resultVector = new double[v1.length];
        for (int i = 0; i < v1.length; i++) {
            resultVector[i] = v1[i]*scalar;
        }
        return resultVector;
    }

    double[] VectorCrossover(double[] v1, double[] v2, CrossoverType crossoverType, double v1Probability) {
        Random r = new Random();
        double[] resultVector = new double[v1.length];
        int startIndex = r.nextInt(v1.length);

        resultVector[startIndex] = v1[startIndex];

        if (crossoverType == CrossoverType.UNIFORM) {
            for (int i = 0; i < v1.length; i++) {
                if (i == startIndex) continue;
                if (r.nextDouble() < v1Probability) {
                    resultVector[i] = v1[i];
                } else {
                    resultVector[i] = v2[i];
                }
            }
        }

        
        if (crossoverType == CrossoverType.EXPONENTIAL) {
            int index = startIndex;
            boolean v2_occured = false;
            while (true) {
                index++;
                index = index % v1.length;
                if (index == startIndex) break;

                if (!v2_occured) {
                    if (r.nextDouble() < v1Probability) {
                        resultVector[index] = v1[index];
                    } else {
                        resultVector[index] = v2[index];
                        v2_occured = true;
                    }
                } else {
                    resultVector[index] = v2[index];
                }

            }
        }

        
        return resultVector;

    }

    double AverageCostFunction(double[][] population) {

        double sum = 0;

        for (int i = 0; i < population.length; i++) {
            sum += costFunc.GetValueAtPoint(population[i])/population.length;
            //System.out.println(costFunc.GetValueAtPoint(population[i])/population.length);
        }

        //System.out.println(sum);
        //System.exit(0);

        return sum;
    }

    double[] ReturnBestSolutionInPopulation(double[][] population) {

        int currentBestIndex = 0;

        for (int i = 1; i < population.length; i++) {
            if (costFunc.GetValueAtPoint(population[i]) < costFunc.GetValueAtPoint(population[currentBestIndex])) {
                currentBestIndex = i;
            }
        }

        return population[currentBestIndex];
    }


}

public class Main {
    public static void main(String[] args) { 
        
        double[] startingSolution = new double[] {7.0f, -3.0f, 2.0f, 1.0f, 3.0f, 3.0f}; 
        /* 
         * {7.0f, -3.0f, 2.0f, 1.0f, 3.0f, 3.0f}
         * {7.0f, -3.0f, 2.0f, 1.0f, -3.0f, 3.0f}
         * imam osjecaj da on ovdje cesto zaglavljuje u lokalnom optimumu
         * 
         * 
        */
        //NumOptAlgorithms.GradientDescent(new TestCostFunc(), 100, startingSolution); 
        //double[] startingSolution = new double[] {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f};


        String path = "02-zad-prijenosna.txt";
        
        F4CostFunc f = new F4CostFunc(path);
        //System.out.println(f.GetValueAtPoint(startingSolution));
        
        //za debugiranje
        Optimizer optimizer = new Optimizer(f, 200, 20000, -5.0, 5.0, 10.0, 0.4, CrossoverType.UNIFORM);
        double[] solution = optimizer.Optimize();

        for(int i = 0; i < solution.length; i++) {
            System.out.print(solution[i] + " ");
        }

    }
}
