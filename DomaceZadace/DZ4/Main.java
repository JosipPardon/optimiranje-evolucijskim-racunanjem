import java.util.*;
import java.util.Random;
import java.util.function.*;
import java.io.IOException;
import java.io.PipedOutputStream;
import java.io.PipedInputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import javax.rmi.ssl.SslRMIClientSocketFactory;
import javax.swing.plaf.synth.SynthSpinnerUI;
import java.lang.Math;
import java.io.*;
import java.math.BigDecimal;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
/* https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/package-summary.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/index.html?org/apache/commons/math3/util/Precision.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/MathUtils.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/Precision.html
*/


/*
 * ZADACI:
 * 
 * OBAVLJENO:
 * implementiraj da imaš unarne operatore i binarne => svaki ima točno definiran broj djece koji mora imati (1 ili 2)
 * 
 */

enum Type {
    operator,
    konstanta,
    varijabla
}

enum OperatorType {
    unary,
    binary
}

enum Method {
    grow,
    full
}

/**
 * Utility for making deep copies (vs. clone()'s shallow copies) of objects
 * in a memory efficient way. Objects are serialized in the calling thread and
 * de-serialized in another thread.
 *
 * Error checking is fairly minimal in this implementation. If an object is
 * encountered that cannot be serialized (or that references an object
 * that cannot be serialized) an error is printed to System.err and
 * null is returned. Depending on your specific application, it might
 * make more sense to have copy(...) re-throw the exception.
 */


class Node {
    
    Double value = null;
    String operator = null;

    Integer variableIndex = null;
    Type type = null;
    OperatorType operatorType = null;
    int requiredNumberOfChildren;
    List<Node> childNodes = Arrays.asList(null, null);
    Node parent = null;

    Double a = 0.0;
    Double b = 1.0;
    

    public Node(Double value, Type type, Integer variableIndex) {
        this.value = value;
        this.type = type;
        if (type == Type.varijabla) {
            this.variableIndex = variableIndex;
        } 
        this.requiredNumberOfChildren = 0;
    }

    public Node(String operator) {
        this.operator = operator;
        this.type = Type.operator;
        if (operator.equals("+") || operator.equals("-") || operator.equals("/") || operator.equals("*")) {
            this.operatorType = OperatorType.binary;
            this.requiredNumberOfChildren = 2;
        } else {
            this.operatorType = OperatorType.unary;
            this.requiredNumberOfChildren = 1;
        } 
    }

    public Double calculateValue() {
        Double returnValue = 0.0;
        if (type != Type.operator) {
            returnValue = value;
        } else {
            try {
                if (operator.equals("+")) {
                    returnValue = childNodes.get(0).calculateValue() + childNodes.get(1).calculateValue();
                }
                if (operator.equals("-")) {
                    returnValue = childNodes.get(0).calculateValue() - childNodes.get(1).calculateValue();
                }
                if (operator.equals("*")) {
                    returnValue = childNodes.get(0).calculateValue() * childNodes.get(1).calculateValue();
                }
                if (operator.equals("/")) {
                    returnValue = childNodes.get(0).calculateValue() / childNodes.get(1).calculateValue();
                }
                if (operator.equals("sin")) {
                    returnValue = Math.sin(childNodes.get(0).calculateValue());
                }
                if (operator.equals("cos")) {
                    returnValue = Math.cos(childNodes.get(0).calculateValue());
                }
                if (operator.equals("sqrt")) {
                    returnValue = Math.sqrt(childNodes.get(0).calculateValue());
                }
                if (operator.equals("log")) {
                    returnValue = Math.log10(childNodes.get(0).calculateValue());
                }
                if (operator.equals("exp")) {
                    returnValue = Math.exp(childNodes.get(0).calculateValue());
                }
            } catch(Exception e) {
                System.out.println(e.toString());
                return 1.0;
            }
   
        }

        if (returnValue.isNaN() || returnValue.isInfinite()) {
            returnValue = 1.0;
        }
        
        return returnValue;

    }

    public void SetA(Double a) {
        this.a = a;
    }

    public void SetB(Double b) {
        this.b = b;
    }

    public Double GetA() {
        return a;
    }

    public Double GetB() {
        return b;
    }

    public Node GetChildAtIndex(int index) {
        return childNodes.get(index);
    }

    public Type GetType() {
        return type;
    }

    public OperatorType GetOperatorType() {
        return operatorType;
    }

    public void SetValue(Double value) {
        this.value = value;
    }

    public void SetParent(Node parent) {
        this.parent = parent;
    }

    public void SetVariableIndex(int index) {
        this.variableIndex = index;
    }

    public int GetVariableIndex() {
        return variableIndex;
    }

    public void addChildren(Node... children) {

        //trebamo pronaći prvo polje sa null pointerom
        int startIndex = 0;
        for (int i = 0; i < childNodes.size(); i++) {
            if (childNodes.get(i) == null) {
                startIndex = i;
                break;
            }
        }
        for (int i = 0; i < children.length; i++) {
                childNodes.set(i+startIndex, children[i]);
                children[i].SetParent(this);
        }

        if (operatorType == OperatorType.unary) {
            childNodes.set(1, null); //drugo dijete postavi na nula
        }
            
    }

    public int GetDepth() {
        int depth = 0;
        Node currentNode = parent;
        while (currentNode != null) {
            currentNode = currentNode.parent;
            depth++;
        }
        return depth;
    }

    public int GetRequiredNumberOfChildren() {
        return requiredNumberOfChildren;
    }

    public List<Node> getAllDescendants() {
        List<Node> descendants = new ArrayList<Node>();
        for (Node child : childNodes) {
            if (child != null) {
                descendants.add(child);
                descendants.addAll(child.getAllDescendants());
            }
        }
        return descendants;
    }

    @Override public String toString() {
        if (type == Type.operator) {
            return String.format("[%s|%.2f]", operator, calculateValue());
        } else if (type == Type.konstanta) {
            return String.format("[c|%.2f]", calculateValue());
        } else {
            return String.format("[x%d|%.2f]", GetVariableIndex(),calculateValue());
        }
       
    }

    public Double SetVariablesAndCalculate(Double[] variables) {

        List<Node> nodes = getAllDescendants();
        nodes.add(this);

        for (Node node : nodes) {
            if (node.GetType() == Type.varijabla && node.GetVariableIndex() < variables.length) {
                int i = node.GetVariableIndex();
                node.SetValue(variables[i]);
            }
        }
        
        return calculateValue();
    }

    public void printTreeRec(String prefix, boolean isTail) {
        String nodeName = this.toString();
        String nodeConnection = isTail ? "|-- " : "|-- ";
        System.out.println(prefix + nodeConnection + nodeName);
        for (int i = 0; i < childNodes.size(); i++) {
            String newPrefix = prefix + (isTail ? "    " : "|   ");
            if (childNodes.get(i) != null) {childNodes.get(i).printTreeRec(newPrefix, i == childNodes.size()-1);}
        }
    }

    public Node DeepCopy() {
        Node newNode;
        if (this.type == Type.operator) {
            newNode = new Node(this.operator.toString());
        } else {
            newNode = new Node(Double.valueOf(this.value), this.type, this.variableIndex);
        }
        for (int i = 0; i < childNodes.size(); i++) {
            if (childNodes.get(i) != null) {
                newNode.addChildren(childNodes.get(i).DeepCopy());
            }
        }
        return newNode;
    }

    public int GetDepthOfFurthestDescendant() {
        List<Node> descendants = getAllDescendants();
        int maxDepth = 0;
        for (Node child : descendants) {
            int childDepth = child.GetDepth();
            if (childDepth > maxDepth) {
                maxDepth = childDepth;
            }
        }
        return maxDepth;
    }

    public static List<Node> TreesCrossover(Node node1, Node node2, int maxDepth) {
        Random r = new Random();

        int failCounter = 0;

        while (true) {

            
            Node node1copy = node1.DeepCopy();
            Node node2copy = node2.DeepCopy();
            List<Node> descendants1 = node1copy.getAllDescendants();
            List<Node> descendants2 = node2copy.getAllDescendants();

            Node subtree1 = descendants1.get(r.nextInt(descendants1.size()));
            Node subtree2 = descendants2.get(r.nextInt(descendants2.size()));

            //System.out.println();
            //System.out.println(subtree1 + " - " + subtree2);
            //System.out.println();

            Node subtree1Parent = subtree1.parent;
            subtree1Parent.childNodes.set(subtree1Parent.childNodes.indexOf(subtree1), null);
            Node subtree2Parent = subtree2.parent;
            subtree2Parent.childNodes.set(subtree2Parent.childNodes.indexOf(subtree2), null);

            subtree1Parent.addChildren(subtree2);
            subtree2Parent.addChildren(subtree1);

            if (node1copy.GetDepthOfFurthestDescendant() > maxDepth || node2copy.GetDepthOfFurthestDescendant() > maxDepth)  {
                failCounter++;
                if (failCounter >= 100) {
                    System.out.println("neuspjeh");
                }
                continue;
                
            }

            

            
            return Arrays.asList(node1copy, node2copy);
        }
        

    }

    public static Node Mutate(Node node, int maxDepth, int variablesNumber, List<String> operators, boolean constantsAllowed, Double minConstant, Double maxConstant) {
        Random r = new Random();
        //System.out.println(node.GetDepthOfFurthestDescendant());
        Node newNode = node.DeepCopy();
        List<Node> descendants = newNode.getAllDescendants();
        Node nodeToRemove = descendants.get(r.nextInt(descendants.size()));
        int nodeToRemoveDepth = nodeToRemove.GetDepth();
        //System.out.println();
        //System.out.println(nodeToRemove);
        Node nodeToRemoveParent = nodeToRemove.parent;
        nodeToRemoveParent.childNodes.set(nodeToRemoveParent.childNodes.indexOf(nodeToRemove), null);

        Node subtree = Machine.CreateTree(Method.grow, true, maxDepth - nodeToRemoveDepth, variablesNumber, operators, constantsAllowed, minConstant, maxConstant);
        nodeToRemoveParent.addChildren(subtree);

        /* if (newNode.GetDepthOfFurthestDescendant() > maxDepth) {

            node.printTreeRec("", false);
            System.out.println(node.GetDepthOfFurthestDescendant());
            System.out.println();
            nodeToRemove.printTreeRec("", false);
            System.out.println();
            subtree.printTreeRec("", false);
            System.out.println();
            newNode.printTreeRec("", false);
            System.out.println();
            System.out.println("mutacija" + newNode.GetDepthOfFurthestDescendant() + " " + (maxDepth - nodeToRemoveDepth));
            System.exit(0);
        } */

        return newNode;

    }
}



class Machine {

    int variablesNumber;
    List<String> operators = Arrays.asList("+", "-", "*", "/", "sin", "cos", "sqrt", "log", "exp");
    boolean useConstants = true;
    Double minConstant = -3.0;
    Double maxConstant = 3.0;
    int populationSize = 500;
    int tournamentSize = 7;
    int costEvaluations = 1000000;
    int costEvaluationsCounter = 0;
    Double reproductionProbability = 0.01;
    Double mutationProbability = 0.3;
    int maxGenerations = 1000;

    int maxTreeDepth = 7;
    boolean useLinearScaling = false;
    

    int N;
    Double[][] x;
    Double[] y;

    Double yAverage = 0.0;

    public Machine(int variablesNumber, String path) {
        this.variablesNumber = variablesNumber;
        try {

            //ovo samo preobroji broj redova
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int examplesCounter = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                
                if (data.startsWith("#")) {
                    continue;
                }
                examplesCounter++;
            }
            N = examplesCounter;

            myReader.close();

            x = new Double[examplesCounter][variablesNumber];
            y = new Double[examplesCounter];

            myObj = new File(path);
            myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                
                String[] dataSplitted = data.split("	"); //pazi, tab je razmak
                for (int j = 0; j < dataSplitted.length-1; j++) {
                    x[i][j] = Double.parseDouble(dataSplitted[j]);
                }
                //System.out.println(dataSplitted[dataSplitted.length-1]);

                y[i] = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                yAverage += y[i];
                i++;
            }

            yAverage = yAverage / N;
        
            /* for (double[] x_i: x) {
                System.out.println(Arrays.toString(x_i));
            }
            System.out.println(Arrays.toString(y)); */
            myReader.close();
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
    }

    public void SetParametersFromFile(String path) {
        try {

            //ovo samo preobroji broj redova
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);

            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                
                if (data.startsWith("FunctionNodes: ")) {
                    String secondPart = data.split(": ")[1];
                    this.operators = Arrays.asList(secondPart.split(", "));
                    System.out.println(Arrays.toString(operators.toArray()));
                } else if (data.startsWith("ConstantRange: ")) {
                    String secondPart = data.split(": ")[1];
                    if (secondPart.startsWith("N/A")) {
                        useConstants = false;
                    } else {
                        useConstants = true;
                        String[] splitted = secondPart.split(", ");
                        minConstant = Double.parseDouble(splitted[0]);
                        maxConstant = Double.parseDouble(splitted[1]);
                    }
                    System.out.println(useConstants);
                    System.out.println(minConstant);
                    System.out.println(maxConstant);
                } else if (data.startsWith("PopulationSize")) {
                    String secondPart = data.split(": ")[1];
                    populationSize = Integer.parseInt(secondPart);
                    System.out.println(populationSize);
                } else if (data.startsWith("TournamentSize")) {
                    String secondPart = data.split(": ")[1];
                    tournamentSize = Integer.parseInt(secondPart);
                    System.out.println(tournamentSize);
                } else if (data.startsWith("CostEvaluations")) {
                    String secondPart = data.split(": ")[1];
                    costEvaluations = Integer.parseInt(secondPart);
                    System.out.println(costEvaluations);
                } else if (data.startsWith("MutationProbability")) {
                    String secondPart = data.split(": ")[1];
                    mutationProbability = Double.parseDouble(secondPart);
                    System.out.println(mutationProbability);
                } else if (data.startsWith("MaxTreeDepth")) {
                    String secondPart = data.split(": ")[1];
                    maxTreeDepth = Integer.parseInt(secondPart);
                    System.out.println(maxTreeDepth);
                } else if (data.startsWith("UseLinearScaling")) {
                    String secondPart = data.split(": ")[1];
                    int num = Integer.parseInt(secondPart);
                    useLinearScaling = num == 1 ? true : false;
                    System.out.println(useLinearScaling);
                }
               
            }

            myReader.close();

        
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
    }

    public Double NodeError(Node node) {
        if (useLinearScaling == true) {
            NodeSetAandB(node);
        }
        Double sum = 0.0;
        for (int i = 0; i < N; i++) {
            
            sum += Math.pow(node.GetA() + node.GetB()*node.SetVariablesAndCalculate(x[i]) - y[i], 2); //ako nema skaliranja getA će biti 0, getB će biti 1 => to su defaulntne vrijednosti, i cijeli taj izraz će biti node.SetVariablesAndCalculate(x[i]) - y[i], dakle kao da nema skaliranja

        }
        sum = sum / 2;
        costEvaluationsCounter++;
        return sum;
    }

    void NodeSetAandB(Node node) {
        Double b = 0.0;
        Double bUpperPart = 0.0; //misli se na gornji i donji dio razlomka
        Double bLowerPart = 0.0;
        Double nodeAverage = 0.0;
        for (int i = 0; i < N; i++) {
            nodeAverage += node.SetVariablesAndCalculate(x[i]);
        }
        nodeAverage = nodeAverage / N;
        for (int i = 0; i < N; i++) {
            bUpperPart += (y[i] - yAverage)*(node.SetVariablesAndCalculate(x[i]) - nodeAverage);
            bLowerPart += Math.pow(node.SetVariablesAndCalculate(x[i]) - nodeAverage, 2);
        }
        b = bUpperPart / bLowerPart;
        Double a = yAverage - b*nodeAverage;
        

        /* if (a.isNaN() || b.isNaN()) { 
            node.printTreeRec("", false);
            System.out.println("nodeAverage = " + nodeAverage);
            System.out.println("bUpperPart = " + bUpperPart);
            System.out.println("bLowerPart = " + bLowerPart);
            System.out.println("a = " + a);
            System.out.println("b = " + b);
            System.exit(0);
        } */

        node.SetA(a);
        node.SetB(b);

        



    }
    void Evolve() {

        /* Node node1 = new Node("+");
        Node node2 = new Node(1.0, Type.varijabla, 0);
        Node node3 = new Node("*");
        Node node4 = new Node("*");
        Node node5 = new Node(1.0, Type.varijabla, 0);
        Node node6 = new Node(1.0, Type.varijabla, 0);
        Node node7 = new Node("sin");
        Node node8 = new Node(1.0, Type.varijabla, 0);
        node1.addChildren(node2, node3);
        node3.addChildren(node4, node7);
        node4.addChildren(node5, node6);
        node7.addChildren(node8);

        //node1 = x0 + x0*x0 * sinx0
        System.out.println(node1.SetVariablesAndCalculate(new Double[]{0.1}));
        node1.printTreeRec("", false);
        System.out.println(NodeError(node1)); */

        //testiranje stvaranja stabala
        /* Node node = Machine.CreateTree(Method.full, true, 3, 3, operators, minConstant, maxConstant);
        node.printTreeRec("", false); */

        //testiranje mutacije:
        /* Node node = Machine.CreateTree(Method.grow, false, 5, 3, operators, minConstant, maxConstant);
        node.printTreeRec("", false);
        Node mutated = Node.Mutate(node, 5, 3, operators, minConstant, maxConstant);
        System.out.println();
        mutated.printTreeRec("", false); */

        //startingPopulation
        List<Node> population = new ArrayList<Node>();
        for (int i = 0; i < 50; i++) {
            for (int maxDepth = 2; maxDepth <= 5; maxDepth++) {
                population.add(Machine.CreateTree(Method.full, false, maxDepth, variablesNumber, operators, useConstants, minConstant, maxConstant));
                population.add(Machine.CreateTree(Method.grow,false, maxDepth, variablesNumber, operators, useConstants, minConstant, maxConstant));
            }
        }

        //System.out.println(GetDepthOfFurthestDescendantOfNode(population));



        for (int i = 0; i < maxGenerations; i++) {
            List<Node> newPopulation = new ArrayList<Node>();
            Node previousBest = bestNode(population);
            newPopulation.add(previousBest);
            while (newPopulation.size() < 500) {
                
                Random r = new Random();
                Double number = r.nextDouble();
                if (number <= reproductionProbability) { //reprodukcija
                    Node newNode = kTournament(tournamentSize, population);
                    newPopulation.add(newNode);  
                } else if (number <= reproductionProbability + mutationProbability) { //mutacija
                    Node newNode = kTournament(tournamentSize, population);
                    Node mutatetNewNode = Node.Mutate(newNode, maxTreeDepth, variablesNumber, operators, useConstants, minConstant, maxConstant);
                    newPopulation.add(mutatetNewNode);
                } else { //krizanje
                    Node newNode1 = kTournament(tournamentSize, population);
                    Node newNode2 = kTournament(tournamentSize, population);
                    List<Node> crossoverNodes = Node.TreesCrossover(newNode1, newNode2, maxTreeDepth);
                    newPopulation.addAll(crossoverNodes);
                }

                /* System.out.println(GetDepthOfFurthestDescendantOfNode(newPopulation));
                if (GetDepthOfFurthestDescendantOfNode(newPopulation) > maxTreeDepth) {
                    System.exit(0);
                } */
            }

            population = newPopulation; 
            /* System.out.println(i);
            if (GetDepthOfFurthestDescendantOfNode(population) > maxTreeDepth) {
                System.exit(0);
            } */
            Node bestNode = bestNode(population);
            bestNode.printTreeRec("", false);
            System.out.println("a = " + bestNode.GetA() + ", b = " + bestNode.GetB());

            System.out.println(NodeError(bestNode));
            //System.out.println("jejej");
            if (costEvaluationsCounter > costEvaluations) {
                System.out.println("Predjen broj cost evualuacija");
                break;
            }

        }
    

        
    }

    int GetDepthOfFurthestDescendantOfNode(List<Node> population) {
        int biggestDepth = 0;
        for (Node node : population) {
            if (node.GetDepthOfFurthestDescendant() > biggestDepth) {
                biggestDepth = node.GetDepthOfFurthestDescendant();
            }
        }
        
        return biggestDepth;
    }

    Node kTournament(int k, List<Node> population) {
        Random r = new Random();
        List<Node> pool = new ArrayList<Node>();
        for (int i = 0; i < k; i++) {
            pool.add(population.get(r.nextInt(population.size())));
        }

        return bestNode(pool);
    }
    Node bestNode(List<Node> population) {
        int bestIndex = 0;
        for (int i = 1; i < population.size(); i++) {
            if (NodeError(population.get(i)) < NodeError(population.get(bestIndex))) {
                bestIndex = i;
            }
        }

        return population.get(bestIndex);
        /* population.get(bestIndex).printTreeRec("", false);
        System.out.println(NodeError(population.get(bestIndex)));  */

    }

    public static Node CreateTree(Method method, boolean rootNodeCanBeTerminal, int maxDepth, int variablesNumber, List<String> operators, boolean constantsAllowed, Double minConstant, Double maxConstant) {
        Random r = new Random();
        
        Node rootNode = Machine.returnRandomNode(maxDepth > 0, rootNodeCanBeTerminal, rootNodeCanBeTerminal && constantsAllowed, operators, minConstant, maxConstant);
        //System.out.println(rootNode);
        List<Node> currentNonTerminalLeaves = new ArrayList<Node>();
        currentNonTerminalLeaves.add(rootNode); //dakle svi listovi koji su nezavršni znakovi => to su samo operatori
        
        //System.out.println(Arrays.toString(currentNonTerminalLeaves.toArray()));
        while (currentNonTerminalLeaves.size() > 0) {
            Node leaf = currentNonTerminalLeaves.get(0);
            currentNonTerminalLeaves.remove(0);
            
            boolean operatorsAllowed = leaf.GetDepth() < maxDepth - 1; //kada list bude leaf.GetDepth() = maxDepth - 1, njegova djeca (koja ce biti na razini maxDepth) će moci biti iskljucivo terminalni znakovi => to znaci da nece moci imati djecu
            boolean terminalsAllowed = true;
            if (method == Method.full && leaf.GetDepth() != maxDepth - 1) {terminalsAllowed = false;}

            for (int i = 0; i < leaf.GetRequiredNumberOfChildren(); i++) {
                Node newNode = Machine.returnRandomNode(operatorsAllowed, terminalsAllowed, terminalsAllowed && constantsAllowed, operators, minConstant, maxConstant);
                if (newNode.GetType() == Type.varijabla) {newNode.SetVariableIndex(r.nextInt(variablesNumber));}
                leaf.addChildren(newNode);
                if (newNode.GetType() == Type.operator) {currentNonTerminalLeaves.add(newNode);}
            }
            
        }

        return rootNode;
    }

    public static Node returnRandomNode(boolean operatorsIncluded, boolean variablesIncluded, boolean constantsIncluded, List<String> operators, Double minConstant, Double maxConstant) {
        Random r = new Random();
        Node returnNode = null;
        while (true) {
            int intRandNumber = r.nextInt(4); //zelimo da je jednaka sansa da se izgenerira nesto terminalno ili nesto neterminalno
            if ((intRandNumber == 0 || intRandNumber == 1) && operatorsIncluded == true) {
                returnNode = new Node(operators.get(r.nextInt(operators.size())));
                break;
            }
            else if (intRandNumber == 2 && variablesIncluded == true) {
                returnNode = new Node(0.0, Type.varijabla, 0);
                break;
            }
            else if (intRandNumber == 3 && constantsIncluded == true) {
                double randomValue = minConstant + (maxConstant - minConstant) * r.nextDouble();
                returnNode = new Node(randomValue, Type.konstanta, null);
                break;
            }
        }
        return returnNode;
    }



}

class Main {

    public static void main(String args[]) {
        
       /*  Node node1 = new Node("*");
        Node node2 = new Node("sin");
        Node node3 = new Node("+");
        Node node4 = new Node(1.0, Type.varijabla, 0);
        Node node5 = new Node(1.0, Type.varijabla, 1);
        Node node6 = new Node(1.0, Type.varijabla, 2);
        node1.addChildren(node2);
        node1.addChildren(node3);
        node2.addChildren(node4);
        node3.addChildren(node5, node6);

        //node1 = sin(x0)*(x1 + x2) 

        System.out.println(node1.SetVariablesAndCalculate(new Double[]{9.0, 4.0, 5.0}));


        Node nodeCopy = node1.DeepCopy();

        nodeCopy.SetVariablesAndCalculate(new Double[]{1.0, 2.0, 3.0}); */
        /* node1.printTreeRec("", false);
        nodeCopy.printTreeRec("", false); */

        /* for (Node i : Node.TreesCrossover(node1, nodeCopy, 4)) {
            i.printTreeRec("", false);
        } */





        //1. zadatak
       /*  Machine machine = new Machine(1, "f1.txt");
        machine.SetParametersFromFile("f1config.txt");
        machine.Evolve(); // probaj staviti u config mutationProbability = 0.14 */
        
        //2. zadatak
        /* Machine machine = new Machine(2, "f2.txt");
        machine.SetParametersFromFile("f2config.txt");
        machine.Evolve(); */

        //3. zadatak
        /* Machine machine = new Machine(1, "f3.txt");
        machine.SetParametersFromFile("f3config.txt");
        machine.Evolve(); */ 

        //1. zadatak sa linear scaling
        /* Machine machine = new Machine(1, "f1.txt");
        machine.SetParametersFromFile("f1configLinear.txt");
        machine.Evolve(); // probaj staviti u config mutationProbability = 0.14 */
        
        //3. zadatak sa linear scaling
        
        Machine machine = new Machine(1, "f3.txt");
        machine.SetParametersFromFile("f3configLinear.txt");
        machine.Evolve(); // => izlaz u ubaci u a + b*izlaz, i to ti je konačni izlaz.
        
    }
}