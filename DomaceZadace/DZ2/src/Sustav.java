import java.io.*;
import java.util.Scanner;
import java.util.Random;

class TestCostFunc implements IFunction {


    /* 
     * 2x0 + 3x1 = 2
     * -x0 + 10x1 = 1
     * rjesenje: (x0, x1) = (17/23, 4/23)
    */
    int variablesNumber = 2;
    double[][] n;
    double[] y;
    

    public TestCostFunc() {
        this.variablesNumber = 2;
        n = new double[][] {{2, 3},{-1, 10}};
        y = new double[] {2,1};
    }

    @Override
    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    @Override
    public double GetValueAtPoint(double[] vector) {
        double sumI = 0.0f;
        for (int i = 0; i < variablesNumber; i++) {

            double sumJ = 0;
            for (int j = 0; j < variablesNumber; j++) {
                sumJ += vector[j]*n[i][j];
            }

            sumI += Math.pow(sumJ-y[i], 2);

        }
        return sumI;
    }

    @Override
    public double[] GetGradientAtPoint(double[] vector) {
        double[] gradientVector = new double[variablesNumber];
        for (int k = 0; k < gradientVector.length; k++) {
            

            double sumI = 0.0f;
            for (int i = 0; i < variablesNumber; i++) {

                double sumJ = 0;
                for (int j = 0; j < variablesNumber; j++) {
                    sumJ += vector[j]*n[i][j];
                }

                sumI += 2*(sumJ-y[i])*n[i][k];

            }

            gradientVector[k] = sumI;



        }
        return gradientVector;
    }
    
}


class F3CostFunc implements IFunction {

    int variablesNumber;
    double[][] n;
    double[] y;
    

    public F3CostFunc(String path) {
        this.variablesNumber = 10;

        try {

            n = new double[variablesNumber][variablesNumber];
            y = new double[variablesNumber];

            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                data = data.substring(1, data.length() - 1);
                String[] dataSplitted = data.split(", ");
                for (int j = 0; j < dataSplitted.length-1; j++) {
                    n[i][j] = Double.parseDouble(dataSplitted[j]);
                }
                y[i] = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                i++;
            }
            System.out.println(n);
            System.out.println(y);
            myReader.close();
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
    }

    @Override
    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    @Override
    public double GetValueAtPoint(double[] vector) {
        double sumI = 0.0f;
        for (int i = 0; i < variablesNumber; i++) {

            double sumJ = 0;
            for (int j = 0; j < variablesNumber; j++) {
                sumJ += vector[j]*n[i][j];
            }

            sumI += Math.pow(sumJ-y[i], 2);

        }
        return sumI;
    }

    @Override
    public double[] GetGradientAtPoint(double[] vector) {
        double[] gradientVector = new double[variablesNumber];
        for (int k = 0; k < gradientVector.length; k++) {
            

            double sumI = 0.0f;
            for (int i = 0; i < variablesNumber; i++) {

                double sumJ = 0;
                for (int j = 0; j < variablesNumber; j++) {
                    sumJ += vector[j]*n[i][j];
                }

                sumI += 2*(sumJ-y[i])*n[i][k];

            }

            gradientVector[k] = sumI;



        }
        return gradientVector;
    }
    
}

public class Sustav {
    public static void main(String[] args) throws Exception { 
        
        double[] startingSolution = new double[10];// {5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f, 5.0f};

        for (int i = 0; i < 10; i++) {
            Random r = new Random();
            double rangeMin = -5;
            double rangeMax = 5;
            startingSolution[i] = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        
        }
        //NumOptAlgorithms.GradientDescent(new TestCostFunc(), 100, startingSolution); 

        int maxIterations = Integer.parseInt(args[0]);
        String path = args[1];
        NumOptAlgorithms.GradientDescent(new F3CostFunc(path), maxIterations, startingSolution);

        /* za dubugiranje:
    
        NumOptAlgorithms.GradientDescent(new F1(), 100, startingSolution); 
        NumOptAlgorithms.GradientDescent(new F3CostFunc("02-zad-sustav.txt"), 10000, startingSolution);
        */
    }
}
