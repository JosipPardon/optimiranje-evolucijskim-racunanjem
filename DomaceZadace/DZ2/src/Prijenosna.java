import java.io.*;
import java.math.BigDecimal;
import java.util.Scanner;

/* https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/package-summary.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/index.html?org/apache/commons/math3/util/Precision.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/MathUtils.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/Precision.html
*/



class F4CostFunc implements IFunction {

    int variablesNumber;
    int N;
    double[][] x;
    double[] y;

    double c = 0.00001;
    

    public F4CostFunc(String path) {
        this.variablesNumber = 6;

        try {

            //ovo samo preobroji broj redova
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int examplesCounter = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                examplesCounter++;
            }
            N = examplesCounter;

            myReader.close();

            x = new double[examplesCounter][5];
            y = new double[examplesCounter];

            myObj = new File(path);
            myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                data = data.substring(1, data.length() - 1);
                String[] dataSplitted = data.split(", ");
                for (int j = 0; j < dataSplitted.length-1; j++) {
                    x[i][j] = Double.parseDouble(dataSplitted[j]);
                }
                //String debugVar2 = dataSplitted[dataSplitted.length-1];
                //BigDecimal DebugVar = new BigDecimal("-1231232171.366999794798722313213");
                /* double debugVar1 = Double.parseDouble("1.2345678912345678912345678");
                Double debugVar2 = Double.parseDouble("1.2345678912345678912345678");
                System.out.println(debugVar1);
                System.out.println(debugVar2);
                double debugVar3 = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                System.out.println(debugVar3); */
                y[i] = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                i++;
            }
            //System.out.println(x);
            //System.out.println(y);
            myReader.close();
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
    }

    @Override
    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    @Override
    public double GetValueAtPoint(double[] v) {
        double sum = 0.0f;
        for (int i = 0; i < N; i++) {
            
            sum += Math.pow(y_function(v, x[i]) - y[i], 2);

        }
        sum = sum / 2;
        return sum;
    }

    @Override
    public double[] GetGradientAtPoint(double[] v) {
        double[] gradientVector = new double[variablesNumber];
        for (int k = 0; k < gradientVector.length; k++) {
            
            double sum = 0.0f;
            for (int i = 0; i < N; i++) {

                double factor = 0;
                switch (k) {
                    case 0:
                        factor = x[i][0];
                        break;
                    case 1:
                        factor = (double)Math.pow(x[i][0],3)*x[i][1];
                        break;
                    case 2:
                        factor = (double)Math.exp(v[3]*x[i][2])*(1+(double)Math.cos(v[4]*x[i][3]));
                        break;
                    case 3:
                        factor = v[2]*(double)Math.exp(v[3]*x[i][2])*(1+(double)Math.cos(v[4]*x[i][3]))*x[i][2];
                        break;
                    case 4:
                        factor = v[2]*(double)Math.exp(v[3]*x[i][2])*(-(double)Math.sin(v[4]*x[i][3]))*x[i][3];
                        break;
                    case 5:
                        factor = x[i][3]*(double)Math.pow(x[i][4],2);
                        break;
                    
                    default:
                        break;
                }

                double debugVar = y_function(v, x[i]) - y[i];
                //System.out.println(i + " " + k + " " + debugVar);
                double debugVar2 = y[i];
                sum += c*(y_function(v, x[i]) - y[i])*factor;

            }

            //System.out.println(k + " " + sum);
            gradientVector[k] = sum;
        



        }
        return gradientVector;
    }

    public static double y_function(double[] v, double[] x) {
        double result = v[0]*x[0] + 
                       v[1]*(double)Math.pow(x[0], 3)*x[1] + 
                       v[2]*(double)Math.exp(v[3]*x[2])*(1 + (double)Math.cos(v[4]*x[3])) + 
                       v[5]*x[3]*(double)Math.pow(x[4],2);

        return result;
        
    }
}

public class Prijenosna {
    public static void main(String[] args) throws Exception { 
        
        double[] startingSolution = new double[] {5.0f, -5.0f, 5.0f, 5.0f, 5.0f, 5.0f}; 
        /* 
         * {7.0f, -3.0f, 2.0f, 1.0f, 3.0f, 3.0f}
         * {7.0f, -3.0f, 2.0f, 1.0f, -3.0f, 3.0f}
         * imam osjecaj da on ovdje cesto zaglavljuje u lokalnom optimumu
         * 
         * 
        */
        //NumOptAlgorithms.GradientDescent(new TestCostFunc(), 100, startingSolution); 
        //double[] startingSolution = new double[] {1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f};

        int maxIterations = Integer.parseInt(args[0]);
        String path = args[1];
        
        NumOptAlgorithms.GradientDescent(new F4CostFunc(path), maxIterations, startingSolution);
        
        //za debugiranje
        //NumOptAlgorithms.GradientDescent(new F4CostFunc("src/02-...."), 1000000, startingSolution);

    }
}
