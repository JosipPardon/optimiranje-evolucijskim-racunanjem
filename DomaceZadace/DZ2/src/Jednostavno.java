
import java.lang.Math;
import java.math.BigDecimal;
import java.util.Random;

interface IFunction {
    public int GetVariablesNumber();
    public double GetValueAtPoint(double[] vector);
    public double[] GetGradientAtPoint(double[] vector);
}

//ova fja prima (x0, x1) i vraća x0^2+(x1-1)^2
class F1 implements IFunction {

    
    int variablesNumber;

    public F1() {
        this.variablesNumber = 2;
    }

    @Override
    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    @Override
    public double GetValueAtPoint(double[] v) {
        return (double)Math.pow(v[0],2) + (double)Math.pow(v[1]-1,2);
    }

    @Override
    public double[] GetGradientAtPoint(double[] v) {
        double[] gradientVector = new double[variablesNumber];
        gradientVector[0] = 2*v[0];
        gradientVector[1] = 2*(v[1]-1);
        return gradientVector;
    }

}

class F2 implements IFunction {

    int variablesNumber;

    public F2() {
        this.variablesNumber = 2;
    }

    @Override
    public int GetVariablesNumber() {

        return this.variablesNumber;
    }

    @Override
    public double GetValueAtPoint(double[] v) {
        return (double)Math.pow(v[0]-1,2) + 10.0f*(double)Math.pow(v[1]-2,2);
    }

    @Override
    public double[] GetGradientAtPoint(double[] v) {
        double[] gradientVector = new double[variablesNumber];
        gradientVector[0] = 2*(v[0]-1);
        gradientVector[1] = 20*(v[1]-2);
        return gradientVector;
    }

}

class NumOptAlgorithms {

    static double tolerance_value = 0.0000000001f;

    public static double[] GradientDescent(IFunction iFunction, int maxIterations, double[] startingSolution) {
        
        double[] x = startingSolution;

        System.out.println("Pocetno stanje");
        System.out.print("tocka: ");
        PrintArray(x);
        System.out.println("vrijednost funkcije u tocki: " + iFunction.GetValueAtPoint(x));
        System.out.println("---------------------------------------------------------");
        for (int i = 0; i < maxIterations; i++) {

            
            
            double[] d = MultiplyArrayByScalar(iFunction.GetGradientAtPoint(x), -1.0);
            
            //System.out.println(String.format("hehe %f %f", Math.abs(d[0]), Math.abs(d[1])));
            boolean dEqualZero = true;
            for (int j = 0; j < d.length; j++) {
                if ((Math.abs(d[j]) > tolerance_value)){
                    dEqualZero = false;
                }
            }
            if (dEqualZero) {
                return x;
            }

            double lambdaStar = CalculateLambdaStar(iFunction, d, x);
            
            x = ArraySum(x, MultiplyArrayByScalar(d, lambdaStar));

            System.out.println("rezultat nakon iteracije: " + (i+1));
            System.out.print("tocka: ");
            PrintArray(x);
            System.out.println("vrijednost funkcije u tocki: " + iFunction.GetValueAtPoint(x));
            System.out.println("---------------------------------------------------------");
            
        }
        return null;
    }

    public static double CalculateLambdaStar(IFunction iFunction, double[] d, double[] x) {
        double lambda_lower = 0.0f;
        double lambda_upper = 1.0f;
        while (true) {
            double[] gradient = iFunction.GetGradientAtPoint(ArraySum(x, MultiplyArrayByScalar(d, lambda_upper)));
            double lambda_derivative = MultiplyArrayByArray(gradient, d);
            if (lambda_derivative < 0) {
                lambda_upper = lambda_upper * 2;
            } else {
                break;
            }
        }

        double lambda = 0;
        double lambda_l = lambda_lower;
        double lambda_u = lambda_upper;
        
        while (true) {
            lambda = (lambda_l + lambda_u) / 2;
            double[] gradient = iFunction.GetGradientAtPoint(ArraySum(x, MultiplyArrayByScalar(d, lambda)));
            double lambda_derivative = MultiplyArrayByArray(gradient, d);


            if (Math.abs(lambda_l - lambda_u) < tolerance_value) {
                break;
            }
            
            if (lambda_derivative > 0 + tolerance_value) {
                lambda_u = lambda;
            }
            else if (lambda_derivative < 0 - tolerance_value) {
                lambda_l = lambda;
            } 
            else {
                break;
            }

            
        }


        return lambda;
    }

    private static double[] MultiplyArrayByScalar(double[] array, double scalar) {
        double[] returnArray = new double[array.length];
        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = array[i] * scalar;
        }
        return returnArray;
    }

    private static double MultiplyArrayByArray(double[] array1, double[] array2) {
        double result = 0.0f;
        for (int i = 0; i < array1.length; i++) {
            result += array1[i] * array2[i];
        }
        return result;
    }

    private static double[] ArraySum(double[] array1, double[] array2) {
        double[] returnArray = new double[array1.length];
        for (int i = 0; i < returnArray.length; i++) {
            returnArray[i] = array1[i] + array2[i];
        }
        return returnArray;
    }



    private static void PrintArray(double[] array) {
        String string = "";
        for (int i = 0; i < array.length; i++) {
            string += Double.toString(array[i]) + " ";
        }
        System.out.println(string);
    }


}

public class Jednostavno {
    public static void main(String[] args) throws Exception {
       
        double[] startingSolution;
        
        if (args.length > 2) {
            startingSolution = new double[] {Double.parseDouble(args[2]), Double.parseDouble(args[3])};
        } else {
            Random r = new Random();
            double rangeMin = -5;
            double rangeMax = 5;
            startingSolution = new double[] {rangeMin + (rangeMax - rangeMin) * r.nextDouble(), rangeMin + (rangeMax - rangeMin) * r.nextDouble()};
        }

        IFunction[] functions = new IFunction[] {new F1(), new F2()};
        
        int maxIterations = Integer.parseInt(args[1]);
        IFunction function = functions[Integer.parseInt(args[0])-1];
        
        NumOptAlgorithms.GradientDescent(function, maxIterations, startingSolution);

        

        //za debugiranje
        /* NumOptAlgorithms.GradientDescent(new F1(), 100, startingSolution);
        NumOptAlgorithms.GradientDescent(new F2(), 100, startingSolution); */ 
       
    } 
    
}



