import java.util.*;
import java.util.function.*;
import javax.swing.plaf.synth.SynthSpinnerUI;
import java.lang.Math;
import java.io.*;
import java.math.BigDecimal;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
/* https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/package-summary.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/index.html?org/apache/commons/math3/util/Precision.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/MathUtils.html
 * https://commons.apache.org/proper/commons-math/javadocs/api-3.6.1/org/apache/commons/math3/util/Precision.html
*/

class SimulatedAnnealing {

    int inputVarsNumber; //broj parametara u funkciji koju minimiziramo => to je ujedno i broj parametara u punishmentFunction
    Function<Double[], Double> punishmentFunction;
    int optimisationDirection; // -1 za minimizaciju, +1 za maksimizaciju
    Double[] currentSolution;
    

    public SimulatedAnnealing(int inputVarsNumber, Function<Double[], Double> func, int optimisationDirection) {
        this.inputVarsNumber = inputVarsNumber;
        this.optimisationDirection = optimisationDirection;

        if (optimisationDirection == -1) {
            punishmentFunction = (inputVars) -> {
                return func.apply(inputVars); //ako minimiziramo funkciju f1, onda je funkcija kazne iznos f1 (što je ona veca, bit ce veca kazna)
            };
        } else {
            punishmentFunction = (inputVars) -> {
                return -func.apply(inputVars); //ako maksimiziramo funkciju f1, onda je funkcija kazne negativan iznos f1 (što je ona veca, bit ce manja kazna)
            };
        }
    }

    public Double[] StartAnnealing(Double T0, BiFunction<Double, Integer, Double> coolDown, int Mk, Function<Double[], Double[]> generateNeighborSolution, int outerLimit) {
        currentSolution = new Double[inputVarsNumber];
        for (int i = 0; i < inputVarsNumber; i++) {
            currentSolution[i] = getRandomNumber(-5.0, 5.0);
        }
        System.out.println(Arrays.toString(currentSolution));
        System.out.println(punishmentFunction.apply(currentSolution));
        
        int k = 0;
        double Tk = coolDown.apply(T0, k);

        while (Tk > 0.000001 && k <= outerLimit) {
            
            for (int m = 0; m < Mk; m++) {
                Double[] neighbourSolution = generateNeighborSolution.apply(currentSolution);
                double deltaPunishment = punishmentFunction.apply(neighbourSolution) - punishmentFunction.apply(currentSolution);
                System.out.println(deltaPunishment);
                
                if (deltaPunishment <= 0) {
                    currentSolution = neighbourSolution;
                } else {
                    boolean acceptNeighbourSolution = ThrowDice(deltaPunishment, Tk);
                    if (acceptNeighbourSolution) {
                        System.out.println("prihvacam losije rjesenje");
                        currentSolution = neighbourSolution;
                    }
                }
                //System.out.println(punishmentFunction.apply(currentSolution));
            }
            k++;
            Tk = coolDown.apply(T0, k);
            System.out.println(Tk);
            System.out.println(k);
        }
        
        return currentSolution;
    }

    boolean ThrowDice(double deltaPunishment, double Tk) {
        double randomNumber = Math.random();

        System.out.println(Math.exp(-deltaPunishment/Tk));
        System.exit(0);
        return (randomNumber <= Math.exp(-deltaPunishment/Tk));
    }

    Double ChooseT0() {
        return 10000.0;
    }

    public static class CoolDown_generator {

        public static BiFunction<Double, Integer, Double> Linear(Double beta) {
            BiFunction<Double, Integer, Double> linear = (T0, k) -> {
                return T0 - k*beta;
            };    
            return linear;
        }

        public static BiFunction<Double, Integer, Double> Geometric(Double alfa) {
            BiFunction<Double, Integer, Double> linear = (T0, k) -> {
                return Math.pow(alfa, k)*T0;
            };    
            return linear;
        }
    }

    public static class GenerateNeighborSolution_generator {
        
        public static Function<Double[], Double[]> ChangeEveryInputVar() {
            Function<Double[], Double[]> func = (solution) -> {
                Double[] neighbourSolution = Arrays.copyOf(solution, solution.length);
                for (int i = 0; i < neighbourSolution.length; i++) {
                    neighbourSolution[i] += getRandomNumber(-0.01, 0.01);
                }
                return neighbourSolution;
            };
            return func;
        }

        public static Function<Double[], Double[]> Special() {
            Function<Double[], Double[]> func = (solution) -> {

                int randomIndex = ThreadLocalRandom.current().nextInt(0, 5 + 1);
                Double[] neighbourSolution = Arrays.copyOf(solution, solution.length);
                //System.out.println(randomIndex);
                if (randomIndex == 3) {
                    neighbourSolution[randomIndex] += getRandomNumber(-0.01, 0.01);
                } else {
                    neighbourSolution[randomIndex] += getRandomNumber(-0.1, 0.1);
                }
                return neighbourSolution;
            };
            return func;
        }
    }
    

    public static Double getRandomNumber(Double min, Double max) {
        return ((Math.random() * (max - min)) + min);
    }

    public Double getPunishmentValue() {
        return punishmentFunction.apply(currentSolution);
    }


}


class F4CostFunc {

    int variablesNumber;
    int N;
    Double[][] x;
    Double[] y;

    public double c = 0.00001;
    
    public F4CostFunc(String path) {
        this.variablesNumber = 6;

        try {

            //ovo samo preobroji broj redova
            File myObj = new File(path);
            Scanner myReader = new Scanner(myObj);
            int examplesCounter = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                examplesCounter++;
            }
            N = examplesCounter;

            myReader.close();

            x = new Double[examplesCounter][5];
            y = new Double[examplesCounter];

            myObj = new File(path);
            myReader = new Scanner(myObj);
            int i = 0;
            while (myReader.hasNextLine()) {

                String data = myReader.nextLine();
                if (data.startsWith("#")) {
                    continue;
                }
                data = data.substring(1, data.length() - 1);
                String[] dataSplitted = data.split(", ");
                for (int j = 0; j < dataSplitted.length-1; j++) {
                    x[i][j] = Double.parseDouble(dataSplitted[j]);
                }
                //String debugVar2 = dataSplitted[dataSplitted.length-1];
                //BigDecimal DebugVar = new BigDecimal("-1231232171.366999794798722313213");
                /* double debugVar1 = Double.parseDouble("1.2345678912345678912345678");
                Double debugVar2 = Double.parseDouble("1.2345678912345678912345678");
                System.out.println(debugVar1);
                System.out.println(debugVar2);
                double debugVar3 = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                System.out.println(debugVar3); */
                y[i] = Double.parseDouble(dataSplitted[dataSplitted.length-1]);
                i++;
            }
            //System.out.println(x);
            //System.out.println(y);
            myReader.close();
          } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
          }
        
    }

    public int GetVariablesNumber() {
        return this.variablesNumber;
    }

    
    public Double GetValueAtPoint(Double[] v) {
        double sum = 0.0f;
        for (int i = 0; i < N; i++) {
            sum += c*Math.pow(y_function(v, x[i]) - y[i], 2);
        }
        sum = sum / N; //ne moramo dijeliti sa dva, možemo sa N, ili s bilo čime ili s ničime => svejedno je, mimium je u istoj točki
        return sum;
    }

    public static Double y_function(Double[] v, Double[] x) {
        double result = v[0]*x[0] + 
                       v[1]*Math.pow(x[0], 3)*x[1] + 
                       v[2]*Math.exp(v[3]*x[2])*(1 + Math.cos(v[4]*x[3])) + 
                       v[5]*x[3]*Math.pow(x[4], 2);
        return result;
        
    }
}

class Main {

    public static void main(String args[]) {
        

        Function<Double[], Double> testfunc = inputVars -> {
            return Math.pow(inputVars[0],2) + Math.pow(inputVars[1] - 1.0,2);
        };

        Double[] testInputVars = new Double[]{1.0,2.0};
        //System.out.println(testfunc.apply(testInputVars));
        
        // za sljedece probleme je dobro: Mk = 30000, T0 = 0.01, coolDown = CoolDownFunctionGenerator.Linear(0.005);

        Double[] result;
        Function<Double[], Double> func;
        SimulatedAnnealing sa;
        /* 
        //p1
        func = inputVars -> {
            return Math.pow(inputVars[0],2) + Math.pow(inputVars[1] - 1.0,2);
        };
        
        sa = new SimulatedAnnealing(2, func, -1);
        result = sa.StartAnnealing(SimulatedAnnealing.CoolDown_generator.Linear(0.005), 30000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar());
        System.out.println(Arrays.toString(result));
        
        //p2
        func = inputVars -> {
            return Math.pow(inputVars[0] + 3,2) + Math.pow(inputVars[1] - 7.0,2);
        };

        sa = new SimulatedAnnealing(2, func, -1);
        result = sa.StartAnnealing(SimulatedAnnealing.CoolDown_generator.Linear(0.005), 30000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar());
        System.out.println(Arrays.toString(result)); 

        //p3
        func = inputVars -> {
            return (Math.pow(inputVars[0] + 3,2) + Math.pow(inputVars[1] - 7,2) + Math.pow(inputVars[2] - 7,2));
        };

        sa = new SimulatedAnnealing(3, func, -1);
        result = sa.StartAnnealing(SimulatedAnnealing.CoolDown_generator.Linear(0.005), 30000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar());
        System.out.println(Arrays.toString(result));
        

        //p4
        func = inputVars -> {
            return -(Math.pow(inputVars[0] + 3,2) + Math.pow(inputVars[1] - 7,2) + Math.pow(inputVars[2] - 7,2));
        };

        sa = new SimulatedAnnealing(3, func, 1);
        result = sa.StartAnnealing(SimulatedAnnealing.CoolDown_generator.Linear(0.005), 30000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar());
        System.out.println(Arrays.toString(result));
        System.out.println(sa.getPunishmentValue());  
        */
        
        

        F4CostFunc costFunc = new F4CostFunc("02-zad-prijenosna.txt");

        Function<Double[], Double> prijenosnaError = (v) -> {
            return costFunc.GetValueAtPoint(v);
        }; 

        //System.out.println(prijenosnaError.apply(new Double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0})); 

        sa = new SimulatedAnnealing(6, prijenosnaError, -1); //za T0 = 0.0001 je nekako vjerojatnost prihavacanja slabije temperature oko 60%
        
        // ovo radi okej, više puta pokreni, kad tad ce ispast nesto sto izgleda relativno dobro:
        /* result = sa.StartAnnealing(100.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.5), 20000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar(), 100);
        System.out.println(Arrays.toString(result)); */

        // ovo radi SOLIDNO, više puta pokreni, kad tad ce ispast nesto sto izgleda relativno dobro:
        Double[] currentBestSolution = new Double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0};
        Double[] currentSolution = new Double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0};

        //ovo je okej:
        //currentSolution = sa.StartAnnealing(100.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.2), 40000, SimulatedAnnealing.GenerateNeighborSolution_generator.Special(), 100);
        //jos bolje:
        //currentSolution = sa.StartAnnealing(50.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.2), 40000, SimulatedAnnealing.GenerateNeighborSolution_generator.Special(), 100);
        //ovo je dobro:
        //currentSolution = sa.StartAnnealing(100.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.5), 20000, SimulatedAnnealing.GenerateNeighborSolution_generator.Special(), 100);
        //ovo je show:
        //currentSolution = sa.StartAnnealing(0.0001, SimulatedAnnealing.CoolDown_generator.Geometric(0.9), 20000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar(), 100);
        //ovo je najbolje dosad:
        currentSolution = sa.StartAnnealing(5000.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.95), 20000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar(), 100);
        System.out.println(Arrays.toString(currentSolution));
        System.out.println(prijenosnaError.apply(currentSolution)*(1/costFunc.c));

        //za ovo je iterator izracunao da je average pogreska 9:
        //currentSolution = sa.StartAnnealing(0.16578241, SimulatedAnnealing.CoolDown_generator.Geometric(0.85736505), 34394, SimulatedAnnealing.GenerateNeighborSolution_generator.Special(), 100);
        /* System.out.println(Arrays.toString(currentSolution));
        System.out.println(prijenosnaError.apply(currentSolution)*(1/costFunc.c)); */


        /* for (int i = 0; i < 10; i++) {
            currentSolution = sa.StartAnnealing(100.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.75), 20000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar(), 100);
            
            if (prijenosnaError.apply(currentSolution)*(1/costFunc.c) < (prijenosnaError.apply(currentBestSolution)*(1/costFunc.c))) {
                    currentBestSolution = currentSolution;
            } ;

            System.out.println(i);

        }

        System.out.println(Arrays.toString(currentBestSolution));
        System.out.println(prijenosnaError.apply(currentBestSolution)*(1/costFunc.c)); */

    
        /* for (int i = 0; i < 100; i++) {

            Double T0 = SimulatedAnnealing.getRandomNumber(0.0,1.0);
            Double alfa_  = SimulatedAnnealing.getRandomNumber(0.0,1.0);
            int iterations = ThreadLocalRandom.current().nextInt(0, 40000 + 1);

            System.out.println(T0);
            System.out.println(alfa_);
            System.out.println(iterations);

            Double errorsSum = 0.0;
            for (int j = 0; j < 10; j++) {
                currentSolution = sa.StartAnnealing(T0, SimulatedAnnealing.CoolDown_generator.Geometric(alfa_), iterations, SimulatedAnnealing.GenerateNeighborSolution_generator.Special(), 1000);
                Double error = prijenosnaError.apply(currentSolution)*(1/costFunc.c);
                errorsSum += error;
            }
            
            
            System.out.println(Arrays.toString(currentSolution));
            System.out.println(errorsSum/10); //average
            System.out.println("--------------------------------");

        } */

        


        /* result = sa.StartAnnealing(100.0, SimulatedAnnealing.CoolDown_generator.Geometric(0.25), 40000, SimulatedAnnealing.GenerateNeighborSolution_generator.ChangeEveryInputVar(), 200);
        System.out.println(Arrays.toString(result)); */

        
       
        




    }
}